(function($){
    const api_url = 'http://vietlott-hdg.vn';
    var ONE_NUMBER_PRICE = 10000,
        total, btnAdd, pickingCurrent, tblChosenNumber,
        frmUserInfo, btnCheckout, body, picking;

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        window.open = cordova.InAppBrowser.open;
    }

    $(document).ready(function($) {
        total = $('#total-prize');
        btnAdd = $('.picking-add');
        pickingCurrent = $('.picking-current');
        tblChosenNumber = $('#chosen-numbers').find('tbody');
        frmUserInfo = $('#user-info');
        btnCheckout = $('#checkout > a:first');
        body = $('body');
        picking = $('#picking iframe');
        pageView();
        // pickup();
        // removeFromList();
        // checkoutPopup();
        // checkout();
        // dependentDropdown();
        // indexData();
        history();
    });

    function pageView(){
        var pages = $('#pages'),
            nav = $('.item'),
            navigation = $('#navigation'),
            // btnPicking = $('.item.picking'),
            btnStore = $('.item.store'),
            btnIndex = $('.item.index'),
            btnWebsite = $('.item.website'),
            btnFB = $('.logo .header-link');

        pages.width(window.innerWidth*2)
        pages.height(window.innerHeight - navigation.height())
        // picking.css('height', window.innerHeight - navigation.height());
        btnStore.on('touchstart', function(e) {
            pages.css('left', -window.innerWidth);
            nav.removeClass('active');
            $(this).addClass('active');
        });
        btnIndex.on('touchstart', function(e) {
            pages.css('left', 0)
            nav.removeClass('active');
            $(this).addClass('active');
        });
        btnWebsite.on('touchstart' , function(e) {
            window.open('http://vietlott-hdg.vn/', '_system');
        });
        // btnPicking.on('touchstart', function(e) {
        //     window.open('http://vietlott-hdg.vn/user/picking', '_blank');
        // });


        btnFB.on('touchstart', function(e) {
            window.open('https://www.facebook.com/vietlottxosodientoan', '_system');
            return false;
        });
    }

    /*function indexSlider(){
        // $('.slick-arrow').click(function(){
        //     console.log(1);
        //     indexActive = $('.slick-02 .slick-active').attr("data-slick-index");
        //     console.log(indexActive);
        // });
        var prizes = $('.slick-02'),
            drawing = $('#number_01'),
            drawingDate = $('#date'),
            topPrize = $('#money_special');
        $('.banner-slide').slick({
            dots: true,
            autoplay: true,
        });
        var slick = prizes.slick({
            // autoplay: false,
            fade: false,
            // speed: 1000,
            infinite: false,
            rtl: true

        });
        // console.log(slick)
        // slick.slickGoTo(2);
        prizes.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            drawing.text(json[nextSlide].id);
            drawingDate.text(json[nextSlide].date);
            topPrize.text(json[nextSlide].cash);
        });
    }*/


    /*function pickup(){
        var numbers = $('.number__btn'),
            chosen = 0;

        numbers.on('touchstart', function(){
            if(chosen > 6) return;

            var that = $(this);
            that.toggleClass('number--active');
            if(that.hasClass('number--active')) chosen++;
            else chosen--;
            if(chosen == 6) {
                chosen++; // chosen = 7
                setTimeout(function(){
                    var isAdded = addToList();
                    if(isAdded) {
                        // updatePicking(chosen);
                        chosen = 0;
                    }
                }, 800);
            }
        });
    }*/

    /*function addToList(){
        var eleActive = $('.number--active'),
            numberOfItemsInList = tblChosenNumber.find('tr:not(.null)').length,
            template = '<tr><td>%no</td><td>%0</td><td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td><a href="javascript:;" class="btn-del fa fa-times"></a></td></tr>',
            emptyRow = tblChosenNumber.find('.null');

        if(eleActive.length < 6) {
            navigator.notification.alert("Bạn phải chọn đủ 6 số", false, "Thông báo", "OK");
            return false;
        }
        if(emptyRow.length > 0) {
            emptyRow.remove();
        }


        template = template.replace('%no', numberOfItemsInList + 1 + '.');
        for (var i = 0; i < eleActive.length; i++) {
            var that = $(eleActive[i]),
                number = that.text();
            template = template.replace('%'+i, number);
        };
        tblChosenNumber.append($(template));
        eleActive.removeClass('number--active');
        updateTotal();
        return true;
    }*/

    /*function removeFromList() {
        $('body').on('touchstart', '#chosen-numbers .btn-del', function() {
            var that = $(this),
                tr = that.parents('tr');
            tr.remove();
            updateNoAndTotal();
        })
    }*/

    /*function updateNoAndTotal(){
        var rows = tblChosenNumber.find('tr'),
            tmplEmpty = '<tr class="null"><td>Chưa có số nào được chọn</td></tr>';
        for (var i = 0; i < rows.length; i++) {
            var row = $(rows[i]);
            row.find('td:first').text(i + 1 + '.');
        }
        if(rows.length == 0) {
            tblChosenNumber.append($(tmplEmpty));
        }
        updateTotal();
    }*/

    /*function updateTotal(){
        var numberOfItemsInList = tblChosenNumber.find('tr:not(.null)').length;
        total.text(numberWithDots(numberOfItemsInList * ONE_NUMBER_PRICE));
        if(numberOfItemsInList >= 10) {
            btnCheckout.removeClass('shadow--disabled');
            btnCheckout.addClass('shadow--red');
        } else {
            btnCheckout.removeClass('shadow--red');
            btnCheckout.addClass('shadow--disabled');
        }
    }*/

    function numberWithDots(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    function numberWithoutDots(x) {
        return x.toString().replace('.', '');
    }

    /*function checkoutPopup(){
        var btnClosePopup = $('.popup__close'),
            popup = $('.popup');


        btnCheckout.on('touchstart', function(){
            var rows = tblChosenNumber.find('tr:not(.null)');
            if(rows.length >= 10){
                popup.addClass('active');
                body.addClass('freeze');
            }
            else
                navigator.notification.alert("Bạn phải đặt ít nhất 10 dãy số", false, "Thông báo", "OK");
        });

        btnClosePopup.on('touchstart', removeClassActive);
        // popup.click(removeClassActive);

        function removeClassActive(e){
            popup.removeClass('active');
            body.removeClass('freeze');
        }


        $(document).keydown(function(e) {
            if(e.keyCode == 27) popup.removeClass('active');
        });
    }*/

    /*function checkout() {
        var form = $('#user-info'),
            btnSubmit = $('#btn-submit');


        form.submit(function() {
            if(submitCheckout()){
                btnSubmit.attr('disabled', true);
            }
            return false;
        });

    }*/

    /*function submitCheckout(){
        var fullname = frmUserInfo.find('#full-name'),
            tel = frmUserInfo.find('#tel'),
            //address = frmUserInfo.find('#address'),
            numbers = new Array(),
            rows = tblChosenNumber.find('tr:not(.null)'),
            city = $('#city'),
            district = $('#district'),
            ward = $('#ward'),
            vendor = $('#vendor');

        if(rows.length == 0) {
            navigator.notification.alert("Bạn chưa đặt số nào", false, "Thông báo", "OK");
            return false;
        }
        if(isEmpty(city.val(), true)
            || isEmpty(district.val(), true)
            || isEmpty(ward.val(), true)
            || isEmpty(vendor.val(), true)) {
            navigator.notification.alert("Hãy chọn điểm bán vé gần bạn nhất", false, "Thông báo", "OK");
            return false;
        }
        for (var i = 0; i < rows.length; i++) {
            var row = $(rows[i]),
                tds = row.find('td:not(:first)'),
                number = "";
            for (var j = 0; j < tds.length; j++) {
                number += $(tds[j]).text() + '.';
            }
            number = number.replace(/\.+$/,'');
            numbers.push(number);
        }
        $.ajax({
            url: api_url + '/user/picking/checkout',
            type: 'POST',
            data: {
                name: fullname.val(),
                tel: tel.val(),
                //address: address.val(),
                numbers: numbers,
                // city: city.val(),
                // district: district.val(),
                // ward: ward.val(),
                vendor: vendor.val()
            },
        })
        .done(function(data) {
            if(data.success){
                $('.popup__close').trigger('touchstart');
                navigator.notification.alert("Bạn đã đặt số thành công!", false, "Thông báo", "OK");
                clear();
            }
        })
        .always(function() {
            $('#btn-submit').attr('disabled', false);
        });

        return true;

    }*/

    /*function clear() {
        var tmplEmpty = '<tr class="null"><td>Chưa có số nào được chọn</td></tr>';
        tblChosenNumber.find('tr').remove();
        tblChosenNumber.append($(tmplEmpty));
        updateTotal();
    }*/

    /*function dependentDropdown(){
        var districtUrl = api_url + '/user/dropdown/districts/';
            wardUrl = api_url + '/user/dropdown/wards/';
            streetUrl = api_url + '/user/dropdown/streets/';
            city = $('#city'),
            district = $('#district'),
            ward = $('#ward'),
            vendor = $('#vendor');

        $.ajax({
            url: api_url + '/user/dropdown/cities',
            type: 'GET'
        })
        .done(function(data) {
            var options = "",
                city = $('#city');
            data = data.output;
            for (var i = 0; i < data.length; i++) {
                options += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
            };
            city.html(options);

            district.depdrop({
            depends: ['city'],
                url: api_url + '/user/dropdown/districts',
                initialize: true,
                array: ['city'],
                loadingText: 'Hãy chọn tỉnh/thành',
                placeholder: 'Chọn...'
            });
            ward.depdrop({
                depends: ['city', 'district'],
                url: api_url + '/user/dropdown/wards',
                loadingText: 'Hãy chọn quận/huyện',
                placeholder: 'Chọn...'
            });
            vendor.depdrop({
                depends: ['city', 'district', 'ward'],
                url: api_url + '/user/dropdown/ward_vedors',
                loadingText: 'Hãy chọn phường/xã',
                placeholder: 'Chọn...'
            });
        })
    }*/

    function indexData(data) {
        var jackpotDate = $('#jackpot__date'),
            prize = $('#jackpot__prize'),
            drawing = $('.result__id'),
            resultDate = $('.result__date'),
        strDate = data.date;
        jackpotDate.text(strDate);
        drawing.text(data.id);
        resultDate.text(strDate);
        prize.text(data.cash);

        var today = new Date();
        var day_of_week = today.getDay();
        var time_int = parseInt(today.getHours() + '' + today.getMinutes() + '' + today.getSeconds());
        var days_to_add = 0;
        switch(day_of_week){
            case 0: // CN
            if(time_int >= 174500)
                days_to_add = 3;
            break;
            case 1: // 2
            days_to_add = 2;
            break;
            case 2:
            days_to_add = 1;
            break;
            case 3: // 4
            if(time_int >= 174500)
                days_to_add = 2;
            break;
            case 4:
            days_to_add = 1;
            break;
            case 5: // 6
            if(time_int >= 174500)
                days_to_add = 2;
            break;
            case 6:
            days_to_add = 1;
            break;
        }
        var count_down_to = addDays(today, days_to_add);
        count_down_to.setHours(17,45,0,0);
        startTimer(count_down_to);
    }

    function history() {
        $.ajax({
            url: api_url + '/user/index/history',
            type: 'GET'
        })
        .done(function(data) {
            indexData(data[0]);
            var html = "";
            for (var i = 0; i < data.length; i++) {
                var numbers = data[i].numbers.split(".");
                html += '<ul class="content-listnumber">';
                for (var j = 0; j < numbers.length; j++) {
                    html += '<li class="number">' + numbers[j] +'</li>'
                };
                html += '</ul>';
            };
            var history = $('#result__history'),
                jackpot = $('#jackpot__prize'),
                drawing = $('.result__id'),
                drawingDate = $('.result__date');
            history.append(html);
            var slick = history.slick({
                // autoplay: false,
                fade: false,
                // speed: 1000,
                infinite: false,
                rtl: true

            });

            history.on('beforeChange', function(event, slick, currentSlide, nextSlide){
                drawing.text(data[nextSlide].id);
                drawingDate.text(data[nextSlide].date);
                jackpot.text(data[nextSlide].cash);
            });
        });



    }

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function startTimer(end){
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
                clearInterval(timer);
                document.getElementById('days').innerHTML = '00';
                document.getElementById('hours').innerHTML = '00';
                document.getElementById('minutes').innerHTML = '00';
                document.getElementById('seconds').innerHTML = '00';
                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('days').innerHTML = days;
            document.getElementById('hours').innerHTML = hours;
            document.getElementById('minutes').innerHTML = minutes;
            document.getElementById('seconds').innerHTML = seconds;
        }

        timer = setInterval(showRemaining, 1000);
    }

    function isEmpty (value, trim) {
        return value === null || value === undefined || value.length === 0 || (trim && $.trim(value) === '');
    };

    document.addEventListener("backbutton", onBackKeyDown, false);

    function onBackKeyDown(e) {
        var nav = document.getElementById('navigation');
        var first_li = nav.querySelector('li');
        var is_home = (first_li.className.indexOf('active') != -1);
        if(is_home)
            navigator.app.exitApp();
        else{
            var event = document.createEvent('HTMLEvents');
            event.initEvent('touchstart', true, false);
            first_li.dispatchEvent(initEventnt);
        }
    }

})(jQuery)